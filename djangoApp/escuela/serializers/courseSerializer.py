"""Course serializers."""

#Django
from django.contrib.auth import authenticate

#Models
from escuela.models.courses import Course

#Django Rest FRAMEWORK
from django.core.validators import RegexValidator
from rest_framework import serializers



class CourseSerializer(serializers.Serializer):
    """
        Course serializer

    """
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(max_length=50,required=True)
    description = serializers.CharField(max_length=300,required=False)


class CourseSerializerCreate(serializers.Serializer):
    """
        Course serializer

    """
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(max_length=50,required=True)
    description = serializers.CharField(max_length=300,required=False)


    def validate(self,data):
        """Check credentials"""
        test = data.get('id',None)
        if test is None:
            return data
        course = Course.objects.filter(id=test)
        if  course:
            raise serializers.ValidationError('Invalid Data, Check ID ')
        return data


    def create(self,data):
        """Create Course"""
        return Course.objects.create(**data)
"""User serializers."""

#Django
from django.contrib.auth import authenticate

from django.contrib.auth.hashers import make_password
#Models
from escuela.models.users import User

#Django Rest FRAMEWORK
from django.core.validators import RegexValidator
from rest_framework import serializers



class StudentSerializer(serializers.Serializer):
    """
        Student serializer

    """
    username = serializers.CharField(max_length=17,required=True)
    password = serializers.CharField(min_length=8,max_length=64,required=True)
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message='Phone number must be entered in the format: +9999999.'
    )
    email = serializers.EmailField(required=False)
    phone = serializers.CharField(validators=[phone_regex],max_length=17,required=False) 
    address = serializers.CharField(max_length=200,required=False)

   


class StudentSerializerCreate(serializers.Serializer):
    """
        Student serializer

    """
    username = serializers.CharField(max_length=17,required=True)
    password = serializers.CharField(min_length=8,max_length=64,required=True)
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message='Phone number must be entered in the format: +9999999.'
    )
    email = serializers.EmailField(required=False)
    phone = serializers.CharField(validators=[phone_regex],max_length=17,required=False) 
    address = serializers.CharField(max_length=200,required=False)

    def validate(self,data):
        """Check credentials"""
        user = User.objects.filter(username=data['username'])
        if  user:
            raise serializers.ValidationError('Username Already Exists')
        return data

    def create(self,data):
        """Create Student"""
        return User.objects.create(**data)
    
    def validate_password(self, value: str) -> str:
        """
        Hash value passed by user.

        :param value: password of a user
        :return: a hashed version of the password
        """
        return make_password(value)
    
    
#Django
from django.urls import path
from escuela.views import StudentView,CourseView,GradeView,AssignmentView
from escuela.views import GradeStudentView,ReportStudentsView,ReportStudentsAverageView

#URls
urlpatterns = [
    path('api/student/', StudentView.as_view(), name='student'),
    path('api/student/grade/', GradeStudentView.as_view(), name='studentGrade'),
    path('api/course/', CourseView.as_view(), name='course'),
    path('api/grade/', GradeView.as_view(), name='grade'),
    path('api/assignment/', AssignmentView.as_view(), name='assignment'),
    path('api/report/a/', ReportStudentsView.as_view(), name='reportqty'),
    path('api/report/b/', ReportStudentsAverageView.as_view(), name='reportqty'),
]
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

#Django REST Framework
from rest_framework import status
from rest_framework.views import APIView

#Models
from escuela.models.grades import Grade
from escuela.models.assignment import Assignment
#Serializer

class GradeView(APIView):
    #permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        """Handle GET request

            Returns all the Grades
        """
        grades = Grade.objects.all()
        data = {}
        for grade in grades:
            data[grade.id]= {
                'student':str(grade.assignment.student),
                'course':str(grade.assignment.course),
                'assignment':grade.assignment.id,
                'grade':grade.grade
            }
        return Response((data))

    def post(self,request,*args,**kwargs):
        """Handle POST request

            Create a new Grade
        """
        try:
            assignmentValue = Assignment.objects.get(id=request.data.get("assignment","")) 
            grades = request.data["grade"]
            grade = Grade(assignment=assignmentValue,grade=grades)
            grade.save()
            data= {
                'student':str(grade.assignment.student),
                'course':str(grade.assignment.course),
                'grade':grade.grade
            }
            return Response(data,status=status.HTTP_201_CREATED)
        except:
            return Response("json values are not valid",status=status.HTTP_400_BAD_REQUEST)
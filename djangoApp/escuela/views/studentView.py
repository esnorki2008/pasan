from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

#Django REST Framework
from rest_framework import status
from rest_framework.views import APIView

#Models
from escuela.models.users import User
from escuela.models.grades import Grade
from escuela.models.assignment import Assignment
#Serializer
from escuela.serializers.userSerializer import StudentSerializer,StudentSerializerCreate


class GradeStudentView(APIView):
    #permission_classes = (IsAuthenticated,)
    
    def post(self,request,*args,**kwargs):
        """Handle POST request

            Returns all the grades that the student has.
        """
        usr = ""
        try:
            usr = request.data["username"]
        except:
            return Response("User not found",status=status.HTTP_400_BAD_REQUEST)
        student = User.objects.filter(username=usr)
        assignments =  Assignment.objects.filter(student=student[0])
        assignmentsList = {}
        for assignment in assignments:
            grades = Grade.objects.filter(assignment=assignment)
            gradesList = {}
            for grade in grades:
                gradesList[grade.id]= {
                'course':str(grade.assignment.course),
                'assignment':grade.assignment.id,
                'grade':grade.grade
            }
            assignmentsList[assignment.id] = {
                'course':str(assignment.course),
                'student':str(assignment.student),
                'grades':gradesList
            }
        return Response(assignmentsList)


class StudentView(APIView):
    #permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        """Handle GET request

            Returns all the students
        """
        users = User.objects.filter(is_admin=False,is_teacher=False,is_staff=False)
        serializer = StudentSerializer(users,many=True)
        return Response(serializer.data)


    def post(self,request,*args,**kwargs):
        """Handle POT request

            Create a new student
        """
        serializer = StudentSerializerCreate(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = serializer.save()
        return Response(StudentSerializer(token).data,status=status.HTTP_201_CREATED)


    def delete(self,request,*args,**kwargs):
        """Handle delete request

            Delete a student
        """
        data = request.data.get('username',"")
        try:
            respon = User.objects.get(username=data).delete()
        except: 
            return Response("User not found",status=status.HTTP_400_BAD_REQUEST)
        return Response("Student: {}  Deleted ".format(data),status=status.HTTP_204_NO_CONTENT)


    def put(self,request,*args,**kwargs):
        """Handle PUT request

            Update a student
        """
        serializer = StudentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = request.data
       
        query = User.objects.filter(username=data.get('username',""))
        query.update(
                phone=data.get('phone',""),
                address=data.get('address',""),
                email=data.get('email',""),
                password=data.get('password',""),
        )

        if query.count() < 1:
            return Response("User not found for update",status=status.HTTP_404_NOT_FOUND) 
         
        return Response(data,status=status.HTTP_201_CREATED)
    
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

#Django REST Framework
from rest_framework import status
from rest_framework.views import APIView

#Models
from escuela.models.assignment import Assignment
from escuela.models.users import User
from escuela.models.courses import Course
from escuela.models.grades import Grade
#Serializer

class ReportStudentsView(APIView):
    #permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        """Handle GET request

            Returns all the number of students in every course
        """
        courses = Course.objects.all()
        data = {}
        for course in courses:
            numberAssignment = 0
            for assignment in Assignment.objects.filter(course=course):
                numberAssignment = numberAssignment + 1

            data[course.id]= {
                'id':course.id,
                'course':str(course),
                'numberStudents':numberAssignment
            }
        return Response((data))

class ReportStudentsAverageView(APIView):
    #permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        """Handle GET request

            Returns the average grade for every course that every student has
        """
        students = User.objects.filter(is_teacher=False,is_admin=False)
        data = {}
        for student in students:
            assignmentList = {} 
            for assignment in Assignment.objects.filter(student=student):
                numberGrades = 0
                valueGrades = 0
                for grade in Grade.objects.filter(assignment=assignment):
                    valueGrades = valueGrades + grade.grade
                    numberGrades = numberGrades + 1         
                average = 0
                if numberGrades != 0:
                    average = valueGrades/numberGrades
                assignmentList[assignment.course.id] = {
                    'course':assignment.course.name,
                    'average':average
                }

            data[student.id]= {
                'id':student.id,
                'username':student.username,
                'email':student.email,
                'courses':assignmentList,
                
            }
        return Response((data))
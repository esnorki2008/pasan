from .studentView import  StudentView,GradeStudentView
from .courseView import  CourseView
from .gradeView import  GradeView
from .assignmentView import AssignmentView
from .reportView import ReportStudentsView,ReportStudentsAverageView
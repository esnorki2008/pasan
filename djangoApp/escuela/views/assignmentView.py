from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

#Django REST Framework
from rest_framework import status
from rest_framework.views import APIView

#Models
from escuela.models.assignment import Assignment
from escuela.models.users import User
from escuela.models.courses import Course
from escuela.models.grades import Grade
#Serializer

class AssignmentView(APIView):
    #permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        """Handle GET request

            Returns all the Assignment in field grades returns
            all the scores associated with the assignment
        """
        assignments = Assignment.objects.all()
        data = {}
        for assignment in assignments:
            gradesList = {}
            for grade in Grade.objects.filter(assignment=assignment):
                gradesList[grade.id]= {
                    'score':grade.grade
                }
            data[assignment.id]= {
                'student':str(assignment.student),
                'course':str(assignment.course),
                'grades':gradesList
            }
        return Response((data))


    def post(self,request,*args,**kwargs):
        """Handle POST request

            Create a new Grade
        """
        assignment = None
        try:
            studentValue = User.objects.get(username=request.data.get("student","")) 
            courseValue = Course.objects.get(id=request.data.get("course","")) 
            if Assignment.objects.filter(student=studentValue,course=courseValue):
                return Response("a student can only be asigned to one course",status=status.HTTP_400_BAD_REQUEST)
            assignment = Assignment(student=studentValue,course=courseValue)
        except:
            return Response("assignment values are not valid",status=status.HTTP_400_BAD_REQUEST)

        assignment.save()
        data= {
            'student':str(assignment.student),
            'course':str(assignment.course),            
        }
        return Response(data,status=status.HTTP_201_CREATED)
        
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

#Django REST Framework
from rest_framework import status
from rest_framework.views import APIView

#Models
from escuela.models.courses import Course

#Serializer
from escuela.serializers.courseSerializer import CourseSerializer,CourseSerializerCreate

class CourseView(APIView):
    #permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        """Handle GET request

            Returns all the Courses
        """
        course = Course.objects.all()
        serializer = CourseSerializer(course,many=True)
        return Response(serializer.data)


    def post(self,request,*args,**kwargs):
        """Handle POT request

            Create a new Course
        """
        serializer = CourseSerializerCreate(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = serializer.save()
        return Response(CourseSerializer(token).data,status=status.HTTP_201_CREATED)


    def delete(self,request,*args,**kwargs):
        """Handle delete request

            Delete a course
        """
        data = request.data.get('id',0)
        print(data)
        try:
            respon = Course.objects.get(id=data).delete()
        except: 
            return Response("Course not found",status=status.HTTP_400_BAD_REQUEST)
        return Response("Course with ID: {}  Deleted ".format(data),status=status.HTTP_204_NO_CONTENT)


    def put(self,request,*args,**kwargs):
        """Handle PUT request

            Update a course
        """
        serializer = CourseSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data
        query = Course.objects.filter(id=data.get('id',-1))
        query.update(
            name=data.get('name',""),
            description=data.get('description',"")
        )
        if query.count() < 1:
            return Response("Course not found for update",status=status.HTTP_404_NOT_FOUND) 
        return Response(data,status=status.HTTP_201_CREATED)
    
"""User model"""

#Django
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator

#Utilities
from .models  import BaseModel
from .assignment import Assignment


class Grade(BaseModel):
    """ 
        Grade Model
    """

    assignment = models.ForeignKey(
        to=Assignment,on_delete=models.CASCADE
    )


    grade = models.PositiveIntegerField()


    def __str__(self):
        return str(self.grade)

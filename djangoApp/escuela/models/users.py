"""User model"""

#Django
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
#Utilities
from .models  import BaseModel


class User(AbstractUser,BaseModel):
    """ User model.

    App User Model
    """

    username = models.CharField(
        'Username',
        max_length=17,
        error_messages= {
            'unique' : 'A user with that user name already exists.'
        },
        unique= True,
    )
    
    address = models.CharField(
        'Address',
        max_length=200,
        blank=True,
        null=True
    )

    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message='Phone number must be entered in the format: +9999999.'
    )
    phone = models.CharField(validators=[phone_regex],max_length=17,blank=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name','last_name','password']

    is_admin = models.BooleanField(
        'is admin',
        default= False,
        help_text=(
            'admin status'
        )
    )

    is_teacher = models.BooleanField(
        'is teacher',
        default= False,
        help_text=(
            'teacher status'
        )
    )

    def __str__(self):
        return self.username
    
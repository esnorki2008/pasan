"""Assignment model"""

#Django
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator

#Utilities
from .models  import BaseModel
from .users import User
from .courses import Course

class Assignment(BaseModel):
    """ 
        Assignment Model
    """

    student = models.ForeignKey(
        to=User,on_delete=models.CASCADE
    )

    course = models.ForeignKey(
        to=Course,on_delete=models.CASCADE
    )

    

    def __str__(self):
        return str(self.id)

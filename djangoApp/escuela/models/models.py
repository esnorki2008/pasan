"""Base Model"""

#Django
from django.db import models


class BaseModel(models.Model):
    """Base Model with 
    """

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time created'
    )
    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time  modified'
    )

    class Meta:
        """MEta option"""
        abstract = True

        get_latest_by = 'created'
        ordering = ['-created','-modified']
"""User model"""

#Django
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator

#Utilities
from .models  import BaseModel


class Course(BaseModel):
    """ 
        Course Model
    """

    name = models.CharField(
        'name',
        max_length=50,
    )

    description = models.CharField(
        'description',
        help_text=(
            'information about the content of the course'
        ),
        max_length=300,
        blank=True,
    )


    def __str__(self):
        return self.name

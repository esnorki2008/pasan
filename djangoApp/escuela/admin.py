from django.contrib import admin

# Django 
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin


#Models
from escuela.models import User,Grade,Course


@admin.register(User)
class AdminUser(admin.ModelAdmin):
    """User model admin"""
    #list_display = ('reputation','rides_taken','rides_offered')
    #search_fields = ('user__username','user__email','user__first_name','user__last_name')
    #list_filter = ('reputation',)


@admin.register(Course)
class AdminCourse(admin.ModelAdmin):
    """Course model admin"""



@admin.register(Grade)
class AdminGrade(admin.ModelAdmin):
    """Grade model admin"""